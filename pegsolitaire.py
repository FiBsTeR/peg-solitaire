"""
Contains my submission for the Peg Solitaire problem.
"""

__author__ = 'jven@mit.edu (Justin Venezuela)'

from random import randint

class Board(object):
  """
  A mutable representation of a rectangular grid of holes, pegs, and voids.
  Allows for pegs to jump over adjacent pegs into holes.
  """

  def __init__(self, list_board, move_listener = None):
    """
    Constructs a board from a 2D-array representation. list_board should be a
    2D array of 0s, 1s, and 2s, representing holes, pegs, and voids,
    respectively.

    Attributes:
      board_: The 2D representation of the board.
      rows_: The number of rows.
      cols_: The number of columns.
      num_pegs_: The number of pegs in the board.
      move_listener: Optionally, a function to call with the row, column, and
        direction of any move made to the board.
      move_listener_: Optionally, a listener which is notified on any moves made
        to the board. The listener is also notified when the board is finished.
      finished_: Whether the board is finished. No further moves can be made on
        a finished board.
    """
    self.rows_ = len(list_board)
    assert self.rows_ >= 5 and self.rows_ <= 10, 'Invalid number of rows.'
    self.cols_ = len(list_board[0])
    assert self.cols_ >= 5 and self.cols_ <= 10, 'Invalid number of columns.'
    self.board_ = [
        [-1 for col in range(self.cols_)] for row in range(self.rows_)]
    self.num_pegs_ = 0
    for row in range(self.rows_):
      for col in range(self.cols_):
        assert list_board[row][col] in [0, 1, 2], 'Invalid board value.'
        self.board_[row][col] = list_board[row][col]
        self.num_pegs_ += 1 if list_board[row][col] == 1 else 0
    self.move_listener_ = move_listener
    self.finished_ = False

  def __str__(self):
    """ Returns a string representation of the board for debugging.  """
    board_str = ''
    for row in range(self.rows_):
      for col in range(self.cols_):
        if self.board_[row][col] == 0:
          board_str += '-'
        if self.board_[row][col] == 1:
          board_str += '.'
        if self.board_[row][col] == 2:
          board_str += 'x'
      board_str += '\n'
    return board_str

  def get(self, row, col):
    """ Returns the value of the board at the given row and column.  """
    assert self.is_in_bounds(row, col), 'Invalid location for get.'
    return self.board_[row][col]

  def get_num_rows(self):
    """ Returns the number of rows. """
    return self.rows_

  def get_num_cols(self):
    """ Returns the number of columns. """
    return self.cols_

  def move(self, row, col, direction):
    """
    Moves the peg in the specified row and column in the given direction.
    Direction is one of 'u', 'r', 'd', 'l', representing up, right, down, and
    left, respectively.
    """
    assert not self.finished_, 'Cannot make moves to a finished board.'
    assert self.can_move(row, col, direction), 'Invalid move.'
    jump_row, jump_col = self.add_(row, col, direction, 1)
    land_row, land_col = self.add_(row, col, direction, 2)
    self.board_[row][col] = 0
    self.board_[jump_row][jump_col] = 0
    self.board_[land_row][land_col] = 1
    self.num_pegs_ -= 1
    if self.move_listener_ is not None:
      self.move_listener_.notify_move(row, col, direction)

  def finish(self):
    """ Finishes the board, disallowing subsequent moves. """
    assert not self.finished_, 'The board is already finished.'
    self.finished_ = True
    self.move_listener_.notify_finish()

  def can_move(self, row, col, direction):
    """ Returns whether the given move is valid. """
    if not self.is_in_bounds(row, col) or self.get(row, col) != 1:
      return False
    jump_row, jump_col = self.add_(row, col, direction, 1)
    if (not self.is_in_bounds(jump_row, jump_col) or
        self.get(jump_row, jump_col) != 1):
      return False
    land_row, land_col = self.add_(row, col, direction, 2)
    return (self.is_in_bounds(land_row, land_col) and
        self.get(land_row, land_col) == 0)

  def is_in_bounds(self, row, col):
    """
    Returns if the given row and column are in the bounds of this board.
    """
    return row >= 0 and row < self.rows_ and col >= 0 and col < self.cols_

  def add_(self, row, col, direction, distance):
    assert direction in ['u', 'r', 'd', 'l'], 'Invalid direction.'
    if direction == 'u':
      return row - distance, col
    if direction == 'r':
      return row, col + distance
    if direction == 'd':
      return row + distance, col
    if direction == 'l':
      return row, col - distance

class MoveListener(object):
  """
  An object which listens for moves made to a board and outputs them when the
  board is finished.
  """

  def __init__(self):
    """
    Constructor.

    Attributes:
      moves_: A list of strings representing the moves made to the board.
    """
    self.moves_ = []

  def notify_move(self, row, col, direction):
    """ Notifies the listener of a move made to the board. """
    # HackerRank uses (x, y) coordinates instead of (row, col).
    direction_str = ''
    if direction == 'u':
      direction_str = 'UP'
    if direction == 'r':
      direction_str = 'RIGHT'
    if direction == 'd':
      direction_str = 'DOWN'
    if direction == 'l':
      direction_str = 'LEFT'
    self.moves_.append('%d %d %s' % (col, row, direction_str))

  def notify_finish(self):
    """ Notifies the listener that the board is finished. """
    print str(len(self.moves_))
    for move in self.moves_:
      print move

def main():
  num_rows, num_cols = map(int, raw_input().split())
  list_board = [[-1 for col in range(num_cols)] for row in range(num_rows)]
  for row in range(num_rows):
    raw_row = raw_input()
    for col in range(num_cols):
      if raw_row[col] == '-':
        list_board[row][col] = 0
      if raw_row[col] == '.':
        list_board[row][col] = 1
      if raw_row[col] == 'x':
        list_board[row][col] = 2
  board = Board(list_board, move_listener = MoveListener())
  random_solve(board)

def test():
  list_board = [
      [2, 2, 1, 1, 1, 2, 2],
      [2, 2, 1, 1, 1, 2, 2],
      [1, 1, 1, 1, 1, 1, 1],
      [1, 1, 1, 0, 1, 1, 1],
      [1, 1, 1, 1, 1, 1, 1],
      [2, 2, 1, 1, 1, 2, 2],
      [2, 2, 1, 1, 1, 2, 2]]
  board = Board(list_board, move_listener = MoveListener())
  random_solve(board)

def random_solve(board):
  timeout = 1000
  attempt = 0
  while attempt < timeout:
    attempt += 1
    row = randint(0, board.get_num_rows() - 1)
    col = randint(0, board.get_num_cols() - 1)
    if board.get(row, col) != 1:
      continue
    for direction in ['u', 'r', 'd', 'l']:
      if board.can_move(row, col, direction):
        board.move(row, col, direction)
        attempt = 0
        break
  board.finish()

if __name__ == '__main__':
  main()
